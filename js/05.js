
/**
 * [slideBoard ライトボックス内のスライダー用オブジェクト]
 * @constructor
 * @param {Number} duration アニメーションのduration.
 */
var slideBoard = function(duration, easing) {
	/**
	 * [アニメーション]
	 * @type {number}
	 */
	this.duration = duration;
	/**
	 * [アニメーション]
	 * @type {String}
	 */
	this.easing = easing;
	/**
	 * [スライドのカレントナンバー]
	 * @type {number}
	 */
	this.count = 0;
	/**
	 * [クリックフラグ]
	 * @type {number}
	 */
	this.flag = true;
};

/**
 * [init スライダーの初期セットアップ]
 */
slideBoard.prototype.init = function(target) {
	this.$target = $(target);
	this.$imgList = this.$target.find('li');
	this.listCount = this.$imgList.length;
	this.$target.attr('id', 'js-slideImg');
	this.$target.find('li').css('opacity', 0);
	this.$target.find('li').eq(0).css('opacity', 1);
};

/**
 * [nextImage スライダーを次へ進める]
 * @param {Object} obj クリックされたボタン要素.
 */
slideBoard.prototype.nextImage = function(obj) {
	var self = this;
	if (self.flag === false) {
		return;
	}
	var max = self.listCount;
	if (self.count === max - 2) {
		$(obj).hide();
	}
	self.$target.find('.js-slidePrev').show();
	self.flag = false;
	self.$imgList.eq(self.count).animate({
		opacity: 0
	}, {
		duration: self.duration,
		easing: self.easing,
		complete: function() {
			self.$imgList.eq(self.count + 1).animate({
				opacity: 1
			}, self.duration).show();
			$(this).hide();
			self.count += 1;
			self.flag = true;
		}
	});
};

/**
 * [prevImage スライダーを前に戻す]
 * @param {Object} obj クリックされたボタン要素.
 */
slideBoard.prototype.prevImage = function(obj) {
	var self = this;
	if (self.flag === false) {
		return;
	}

	if (self.count < 2) {
		$(obj).hide();
	}
	self.$target.find('.js-slideNext').show();
	self.flag = false;
	self.$imgList.eq(self.count).animate({
		opacity: 0
	}, {
		duration: self.duration,
		easing: self.easing,
		complete: function() {
			self.$imgList.eq(self.count - 1).animate({
				opacity: 1
			}, self.duration).show();
			$(this).hide();
			self.count -= 1;
			self.flag = true;
		}
	});
};

var slider = new slideBoard(250, 'swing');

slider.init('#js-slider');

$(document)
// スライドの次へボタン
.delegate('.js-slideNext', 'click', function() {
	slider.nextImage(this);
})
// ライトボックス内、スライドの前へボタン
.delegate('.js-slidePrev', 'click', function() {
	slider.prevImage(this);
});
